# IDS 721 Week 5 Mini Project

> Oliver Chen (yc557)

## Project Introduction
This project focuses on building a serverless Rust microservice using AWS Lambda. The microservice will implement a simple yet impactful service, integrating seamlessly with a database. The core aspects include Rust Lambda functionality, efficient database integration, and the implementation of a practical microservice. Documentation will ensure clarity, and the project's success will be demonstrated through screenshots, accompanied by a detailed write-up explaining the service's architecture and functionality.

## Detailed Setup

### Rust Cargo Lambda Project
1. Install Cago Lambda if haven't already.
```
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
``` 
2. Create a new Cargo Lambda project by running the following command:
```
cargo lambda new yc557-week5-mini-project \
    && cd yc557-week5-mini-project
```
3. Edit files in the project template such as `main.rs` and `Cargo.toml`. In my case, the Rust AWS Lambda function is a simple service that fetches employee names from a DynamoDB database. Users can make requests with the employee's company and role parameters, and the function responds with the selected employee ID and name from the specified criteria. It demonstrates the use of Rust in serverless computing and is designed for straightforward integration with DynamoDB, making it an efficient and user-friendly microservice.
4. Deploy the project to AWS. This is similar to what's done in mini project 2, so repetition is avoided here.
```
cargo lambda build --release
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112660632:role/ids721
```


## AWS Detailed Setup
1. Attach the following policies to the role associated with the AWS Lambda function: `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess`, and `AWSLambdaBasicExecutionRole`.
2. Search for **DynamoDB** on the AWS console. Create a new table named `EmployeeInfo`. Under **Explore items**, create some new items as data for the service. For example, here is one of my data entries in JSON:
```
{
  "name": {
    "S": "Sundar Pichai"
  },
  "company": {
    "S": "Google"
  },
  "employee_role": {
    "S": "CEO"
  },
  "employee_id": {
    "N": "11"
  }
}
```
3. Under the Lambda function, create a trigger, i.e., API gateway for the function. Create a REST API.
4. Create a new resource for the REST API. Specify a resource name such as `week5-resource`. Then add an `ANY` method to the resouce.
5. Deploy the API by clicking the button on the top right. Create a new stage called `test` for instance. Here is the official [guide](https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-deploy-api-with-console.html) from AWS.
6. Then we can find the invoke URL under the newly created stage.


## Demo
### Role Permissions
![role](./readme-images/role.png)

### Rust/AWS Lambda
This is the functional Rust Lambda function deployed on AWS.
![lambda-1](./readme-images/lambda-1.png)
This is the API gateway trigger attached to the lambda function.
![lambda-2](./readme-images/lambda-2.png)

### DynamoDB Usage
This is the table named `EmployeeInfo` created in AWS DynamoDB.
![db-1](./readme-images/db-1.png)
This is some sample data for the table.
![db-2](./readme-images/db-2.png)

### Service
The URL to send the POST request in my case is:
```
https://64utey4gk4.execute-api.us-east-1.amazonaws.com/test/yc557-week5-mini-project/week5-resource
```
We can test the functionality of the service using **Postman** to send POST requests. We search the employee by the parameters `company` and `employee_role`. Then, we get the employee's ID and name back.

![test-1](./readme-images/test-1.png)
![test-2](./readme-images/test-2.png)
![test-3](./readme-images/test-3.png)
